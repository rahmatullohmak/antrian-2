<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\antrian;

class proses extends Controller
{
    //

    public function struk()
    {
        $jum = antrian::count();
        $hasil = DB::select('select * from antrian where nomer = ?', [$jum]);
        // dd($hasil);

        // $hasil[0];
        $hari = date('l');
        $tanggal = date('Y-m-d');
        $jam = date('H:i:s');

        return view('struknya', ['nomer' => $hasil[0], 'tgl' => $tanggal, 'jam' => $jam]);
    }

    public function tambah()
    {


        $randomNumber = rand(1, 1000);
        $jumlahBaris = antrian::count() + 1;


        antrian::create([
            'nomer' => $jumlahBaris,
            'kode' => $randomNumber

        ]);


        return redirect('/petugas');

    }
}
