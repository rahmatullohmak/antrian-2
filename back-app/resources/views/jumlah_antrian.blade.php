@extends('layout.app2')


@section('judul')
    Depan
@endsection
@section('content')
    <header id="header" class="header d-flex align-items-center">

        <div class="container-fluid container-xl d-flex align-items-center justify-content-between">
            <a href="" class="logo d-flex align-items-center">

                <h1>Puskesmas<span>.</span></h1>
            </a>
            <nav id="navbar" class="navbar">
                <ul>

                    <li><a href="/login">login</a></li>
                </ul>
            </nav>

            <i class="mobile-nav-toggle mobile-nav-show bi bi-list"></i>
            <i class="mobile-nav-toggle mobile-nav-hide d-none bi bi-x"></i>

        </div>
    </header>


    <section id="hero" class="hero" style="background-color: white;">
        <div class="container position-relative">
            <div class="row gy-5" data-aos="fade-in">

                <div class="col-xl-3 col-md-6" data-aos="fade-up" data-aos-delay="200">
                    <div class="icon-box" style="background-color: white;">
                        <div class="icon"></div>
                        <h4 style="
    font-size: 130px;
">{{ $nomer }}</h4>
                    </div>
                </div>


                <div
                    class="col-lg-3 order-2 order-lg-1 d-flex flex-column justify-content-center text-center text-lg-start">
                    <h2 style="
    color: #008374;
">Selamaat Datang <span>Antrian Puskesmas</span></h2>
                    <p style="
    color: #008374;
">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit qui unde
                        illum inventore nisi, eum adipisci
                        vitae sunt a pariatur repellat. Quod reprehenderit quaerat iusto omnis, animi nostrum ex suscipit.
                    </p>

                </div>



                <div class="col-lg-6 order-1 order-lg-2">
                    <img src="assets/img/hero-img.svg" class="img-fluid" alt="" data-aos="zoom-out"
                        data-aos-delay="100">
                </div>
            </div>
        </div>



        </div>
    </section>

    <footer id="footer" class="footer">


        <div class="container mt-4">
            <div class="copyright">
                &copy; Copyright <strong><span>Impact</span></strong>. All Rights Reserved
            </div>
            <div class="credits">

                Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
            </div>
        </div>

    </footer>

    <a href="#" class="scroll-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>

    <div id="preloader"></div>

    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendor/aos/aos.js"></script>
    <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
    <script src="assets/vendor/purecounter/purecounter_vanilla.js"></script>
    <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
    <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="assets/vendor/php-email-form/validate.js"></script>

    <script src="assets/js/main.js"></script>
@endsection
