@extends('layout.app2')


@section('judul')
    Pilih Metode
@endsection
@section('content')
    <header id="header" class="header d-flex align-items-center">

        <div class="container-fluid container-xl d-flex align-items-center justify-content-between">
            <a href="index.html" class="logo d-flex align-items-center">

                <h1>Puskesmas<span>.</span></h1>
            </a>
            <nav id="navbar" class="navbar">
                <ul>

                    <li><a href="/logout">logout</a></li>
                </ul>
            </nav>

            <i class="mobile-nav-toggle mobile-nav-show bi bi-list"></i>
            <i class="mobile-nav-toggle mobile-nav-hide d-none bi bi-x"></i>

        </div>
    </header>


    <section id="hero" class="white">
        <div class="container position-relative">
            <div class="row gy-5" data-aos="fade-in">

                <div class="col-xl-3 col-md-6" data-aos="fade-up" data-aos-delay="200"
                    style="
    border: 2px solid green;
">
                    <div class="icon-box teks-center" style="
    margin-top: 56px;
    text-align: center;
">

                        <h4 class="title" style="
    font-size: 37px;
">Antrian ke </h4>
                        <h4 class="title" style="
    font-size: 8pc;
">{{ $nomer }}</h4>
                    </div>
                </div>


                <div
                    class="col-lg-5 order-2 order-lg-1 d-flex flex-column justify-content-center text-center text-lg-start">
                    <h2>Metode Pendaftaraan QR: <span></span></h2>
                    <p>1. pasien diwajibkan untuk melakukan pendaftaran menggunakan sistem antrian Pusekesmas melalui scan
                        kode QR
                        <BR>
                        2. Gunakan kamera ponsel atau aplikasi Google Scan untuk memindai kode QR yang tersedia dilokasi
                        pendaftaran. <br>
                        3. Hasil scan akan mengarah ke browser dan akan menunjukan

                    </p>

                    <h2>Metode Pendaftaraan cetak: <span></span></h2>
                    <p>1.jika pasien memilih metode cetak maka harus mengklik tombol cetak maka dengan sendirinya akan
                        mengarah
                        ke perangkat print dan mencetak nomor antrian tersebut
                    </p>
                </div>

                <div class="col-lg-4 ">
                    <p>Pilih metode antrian</p>

                    <div class="card"
                        style="width: 20rem;border: 2px solid green;height: 15rem;background-color: #e2e2e200;">
                        <a href="/barcode">
                            <div class="card-body">
                                <div class="img" style="margin-left: 1cm;">
                                    <img src="{{ asset('assets/img/barcod.jpeg') }}" class="" alt="...">
                                </div>


                            </div>
                        </a>
                    </div>
                    <br>

                    <div class="card"
                        style="width: 20rem;height: 15rem;border: 2px solid green;background-color: #b0aeae00;">
                        <a href="/struk">
                            <div class="card-body">
                                <div class="img" style="margin-left: 1cm;">
                                    <img src="{{ asset('assets/img/print.jpeg') }}" class="" alt="...">
                                    <h6 class="card-subtitle mb-2 text-body-secondary">

                                    </h6>
                                </div>
                        </a>



                    </div>

                </div>
            </div>


        </div>


        </div>
        </div>



        </div>
    </section>

    <footer id="footer" class="footer">


        <div class="container mt-4">
            <div class="copyright">
                &copy; Copyright <strong><span>Impact</span></strong>. All Rights Reserved
            </div>
            <div class="credits">


            </div>
        </div>

    </footer>

    <a href="#" class="scroll-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>

    <div id="preloader"></div>

    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendor/aos/aos.js"></script>
    <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
    <script src="assets/vendor/purecounter/purecounter_vanilla.js"></script>
    <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
    <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="assets/vendor/php-email-form/validate.js"></script>

    <script src="assets/js/main.js"></script>
@endsection
