<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\auth;
use app\Http\Middleware\user;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


// Route::get('/', function () {
//     return view('');
// });


Route::middleware(['guest'])->group(function () {
    // ...
    Route::get('/login', function () {
        return view('login');
    })->name('login');



    Route::post('/login', [App\Http\Controllers\admin::class, 'index']);


    Route::get('/', [App\Http\Controllers\tampil::class, 'index']);



});
Route::middleware(['auth'])->group(function () {
    // ...


    Route::get('/tess', [App\Http\Controllers\admin::class, 'tess']);

    Route::middleware(['user:petugas'])->group(function () {

        Route::get('/admin/petugas', [App\Http\Controllers\admin::class, 'petugas']);

        //tambah
        Route::get('/tambah', [App\Http\Controllers\proses::class, 'tambah']);

        Route::get('/logout', [App\Http\Controllers\admin::class, 'logout']);
        Route::get('/struk', [App\Http\Controllers\proses::class, 'struk']);
    });

    Route::middleware(['user:admin'])->group(function () {


        Route::get('/admin', [App\Http\Controllers\admin::class, 'adantrian']);
        Route::get('/logout_admin', [App\Http\Controllers\admin::class, 'logout_admin']);


    });
});



